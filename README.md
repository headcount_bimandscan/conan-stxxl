# BIM & Scan� Third-Party Library (STXXL)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for [STXXL](http://stxxl.org/), the standard template library for extra-large datasets.

Supports version 1.4.99 (unstable, from Git repo).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) Conan repository for third-party dependencies.
