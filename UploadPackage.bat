@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'README.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "stxxl/1.4.99@bimandscan/unstable"

@ECHO ON
