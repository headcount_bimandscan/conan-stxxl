#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#
cmake_minimum_required(VERSION 3.6 FATAL_ERROR)
project("Conan-STXXL")
message(WARNING "Wrapped version of 'STXXL' for Conan build/deployment.")

include("${CMAKE_BINARY_DIR}/conanbuildinfo.cmake")
conan_basic_setup()

list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_CURRENT_SOURCE_DIR})

# Force loading 'Boost' modules before 'STXXL' attempts to:
find_package(Boost COMPONENTS system
                              thread
                              chrono
                              date_time
                              filesystem
                              iostreams REQUIRED)

# Build library:
add_subdirectory(stxxl_src)
