#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
from conans.model.conan_file import ConanFile

from conans import CMake, \
                   tools


class STXXL(ConanFile):
    name = "stxxl"
    version = "1.4.99"
    license = "BSL-1.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-stxxl"
    description = "Standard template library for extra-large datasets."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "http://stxxl.org"

    _src_dir = "stxxl_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ],
                  "with_gnu_extensions": [
                                             True,
                                             False
                                         ]
              }

    default_options = "shared=False", \
                      "fPIC=True", \
                      "with_gnu_extensions=True"

    exports = "../LICENCE.md"
    exports_sources = "CMakeLists.txt"

    requires = "boost_thread/1.69.0@bincrafters/stable", \
               "boost_system/1.69.0@bincrafters/stable", \
               "boost_random/1.69.0@bincrafters/stable", \
               "boost_date_time/1.69.0@bincrafters/stable", \
               "boost_filesystem/1.69.0@bincrafters/stable", \
               "boost_chrono/1.69.0@bincrafters/stable", \
               "boost_iostreams/1.69.0@bincrafters/stable"

    build_requires = "cmake_findboost_modular/1.69.0@bincrafters/stable"

    def _latest_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

        if self.settings.compiler != "gcc":
            del self.options.with_gnu_extensions

    def source(self):
        git_uri = "https://github.com/stxxl/stxxl.git"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "b9e44f0ecba7d7111fbb33f3330c3e53f2b75236") # commit hash -> 15/11/2018

        # Fix CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "PROJECT_BINARY_DIR",
                              "CMAKE_BINARY_DIR")

        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(local)",
                              "#add_subdirectory(local)")

        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(tools)",
                              "#add_subdirectory(tools)")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["BUILD_STATIC_LIBS"] = not self.options.shared
        cmake.definitions["BUILD_TESTING"] = False
        cmake.definitions["BUILD_TESTS"] = False
        cmake.definitions["USE_OPENMP"] = True # always use OpenMP in this package build
        cmake.definitions["USE_BOOST"] = True # force boost usage
        cmake.definitions["NO_CXX11"] = not self._latest_cppstd()
        cmake.definitions["USE_STD_THREADS"] = self._latest_cppstd()
        cmake.definitions["BUILD_EXAMPLES"] = False
        cmake.definitions["BUILD_EXTRAS"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        if self.settings.compiler == "gcc":
            cmake.definitions["USE_GNU_PARALLEL"] = self.options.with_gnu_extensions

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE_1_0.txt",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.append("/openmp")
            self.cpp_info.cflags.append("/openmp")
        else:
            self.cpp_info.cppflags.append("-fopenmp")
            self.cpp_info.cflags.append("-fopenmp")

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
