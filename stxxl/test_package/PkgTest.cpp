/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <stxxl/vector>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'STXXL' package test (compilation, linking, and execution).\n";

    static const std::size_t vec_size = 100;
    stxxl::vector<int> int_vec(vec_size);

    for (std::size_t i = 0; i < vec_size; ++i)
    {
        int_vec[i] = static_cast<int>(i);
    }

    std::cout << "'STXXL' package works!" << std::endl;
    return EXIT_SUCCESS;
}
